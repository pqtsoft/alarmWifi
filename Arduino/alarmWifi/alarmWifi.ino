#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <EEPROM.h>

#include <Wire.h> 
#include <RtcDS3231.h>

#define address 0x50

RtcDS3231<TwoWire> Rtc(Wire);

IPAddress local_IP(192, 168, 4, 22);
IPAddress gateway(192, 168, 4, 9);
IPAddress subnet(255, 255, 255, 0);
ESP8266WebServer server(80);


const char *SSID = "Arduino Wifi";
const int numberOfFeedTimes = 78;
unsigned int feedTime[numberOfFeedTimes * 3];

#define countof(a) (sizeof(a) / sizeof(a[0]))


// id | gio_phut_thu | chedo_congra_onoff
// thu = 1 -> 8
// gio = 00 -> 25
// phut = 00 -> 61
// chedo = 1 -> 9
// congra = 01 -> 16
// onoff = 0 -> 1
void readThuGioPhut(int &thu, int &gio, int &phut, int input) {
    thu = input % 10;
    input /= 10;
    phut = (input / 10 % 10) * 10 + input % 10;
    input /= 100;
    gio = (input / 10 % 10) * 10 + input % 10;
}
void readCheDoCongRaOnOff(int &chedo, int &congra, int &onoff, int input) {
  if(input < 1000)
  {
    chedo = 1;
    congra = 1;
    onoff = 0;
    return;
  }

  onoff = input % 10;
  congra = (input / 100 % 10) * 10 + (input / 10 % 10);
  chedo = input / 1000;
}

void EEPROMWriteInt(int p_address, unsigned int p_value)
{
 byte lowByte = ((p_value >> 0) & 0xFF);
 byte highByte = ((p_value >> 8) & 0xFF);

 EEPROM.write(p_address, lowByte);
 EEPROM.write(p_address + 1, highByte);
}
unsigned int EEPROMReadInt(int p_address)
{
  byte lowByte = EEPROM.read(p_address);
  byte highByte = EEPROM.read(p_address + 1);
  
  return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}
void ReadFeedTimes() {
     int j = 0;
     for (int i = 0; i < numberOfFeedTimes * 6; i = i + 2)
     {
          feedTime[j++] = EEPROMReadInt(i);
     }
}
void WriteFeedTime(int id, int ThuGioPhut, int CheDoCongRaOnOff) {
     int x = (id - 1) * 6;

     EEPROMWriteInt(x + 2, ThuGioPhut);
     EEPROMWriteInt(x + 4, CheDoCongRaOnOff);
     EEPROM.commit();
     ReadFeedTimes();
}
void setupNewFeedTime() {
     int j = 1;
     for (int i = 0; i < numberOfFeedTimes * 6; i = i + 6) {
       int j = int(i / 78) + 1;
       
       EEPROMWriteInt(i, i / 6 + 1);
       delay(5);
       EEPROMWriteInt(i + 2, j); // 00 - 00 - CN
       delay(5);
       EEPROMWriteInt(i + 4, 1041); // 1 - 16 - 1
       delay(5);
     }
     
     EEPROM.commit();
     ReadFeedTimes();
}
void printDateTime(const RtcDateTime& dt)
{
    char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u %02u/%02u/%04u %02u:%02u:%02u"),
            dt.DayOfWeek(),
            dt.Month(),
            dt.Day(),
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    Serial.print(datestring);
}
void setup()
{
  pinMode(04, OUTPUT);
  pinMode(05, OUTPUT);
  pinMode(16, OUTPUT);
  Wire.begin(0, 2);
  Rtc.Begin();
  EEPROM.begin(1024);

  Serial.begin(115200);
  Serial.println();
  //setupNewFeedTime();
  
  

  Serial.print("compiled: ");
  Serial.print(__DATE__);
  Serial.println(__TIME__);

  ReadFeedTimes();

  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  printDateTime(compiled);
  Serial.println();

  if (!Rtc.IsDateTimeValid()) 
  {
      Serial.println("RTC lost confidence in the DateTime!");
      Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning())
  {
      Serial.println("RTC was not actively running, starting now");
      Rtc.SetIsRunning(true);
  }

  RtcDateTime now = Rtc.GetDateTime();
  if (now < compiled) 
  {
      Serial.println("RTC is older than compile time!  (Updating DateTime)");
      Rtc.SetDateTime(compiled);
  }
  else if (now > compiled) 
  {
      Serial.println("RTC is newer than compile time. (this is expected)");
  }
  else if (now == compiled) 
  {
      Serial.println("RTC is the same as compile time! (not expected but all is fine)");
  }

  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone); 
  
  Serial.print("Setting soft-AP configuration ... ");
  Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");

  Serial.print("Setting soft-AP ... ");
  Serial.println(WiFi.softAP(SSID) ? "Ready" : "Failed!");

  Serial.print("Soft-AP IP address = ");
  Serial.println(WiFi.softAPIP());

  server.on("/", []() {
    String webPage = "";
    webPage += "<h1>ESP8266 Web Server</h1><p>Socket #1 <a href=\"socket1On\"><button>ON</button></a>&nbsp;<a href=\"socket1Off\"><button>OFF</button></a></p>";
    webPage += "<p>Socket #2 <a href=\"socket2On\"><button>ON</button></a>&nbsp;<a href=\"socket2Off\"><button>OFF</button></a></p>";
    server.send(200, "text/html", webPage);
    
  });

  server.on("/check", []() {
    RtcDateTime now = Rtc.GetDateTime();
    String currentHour = now.Hour() < 10 ? "0" + String(now.Hour()) : String(now.Hour());
    String currentMinute = now.Minute() < 10 ? "0" + String(now.Minute()) : String(now.Minute());
    String currentSecond = now.Second() < 10 ? "0" + String(now.Second()) : String(now.Second());
    server.send(200, "text/html", "{\"device\": \"IOTALARM\", \"time\": \"" + currentHour + ":" + currentMinute + ":" + currentSecond +"\",\"day\": \"" + now.Day() + "\", \"month\": \"" + now.Month() + "\", \"year\": \"" + now.Year() + "\", \"thu\": \"" + now.DayOfWeek() + "\"}");  
  });

  server.on("/set", []() {
    String strThuGioPhut = server.arg("thugiophut");
    String strCheDoCongOnOff = server.arg("chedocongonoff");
    String strID = server.arg("id");

    WriteFeedTime( atoi(strID.c_str()), atoi(strThuGioPhut.c_str()), atoi(strCheDoCongOnOff.c_str()) );
    server.send(200, "text/html", "successful");
  });

  server.on("/all", []() {
    String content = "[";
    for (int i = 0; i < numberOfFeedTimes * 3; i = i + 3)
    {
      int thu = 0, gio = 0, phut = 0, chedo = 0, cong = 0, onoff = 0;
      readThuGioPhut(thu, gio, phut, feedTime[i + 1]);
      readCheDoCongRaOnOff(chedo, cong, onoff, feedTime[i + 2]);
      
      content += "{\"id\":" + String( feedTime[i] ) + ",\"dayOfWeek\":" + String(thu) + ",\"hour\":" + String(gio) + ",\"minute\":" + String(phut) + ",\"chedo\":" + String(chedo) + ",\"cong\":" + String(cong) + ",\"onoff\":" + String(onoff) + "}"  + ((i != (numberOfFeedTimes * 3 - 3)) ? "," : "");
    }
    content += "]";
    server.send(200, "text/html", content);
  });
  server.begin();
  Serial.println("HTTP server started");
  digitalWrite(16, HIGH); 
  digitalWrite(04, LOW);
}

void loop() {
  
  Serial.printf("Hour: %d : %d: %d\n", feedTime[0], feedTime[1], feedTime[2]);
  Serial.printf("Stations connected to soft-AP = %d \n", WiFi.softAPgetStationNum());
  server.handleClient();

  if (!Rtc.IsDateTimeValid()) 
    Serial.println("RTC lost confidence in the DateTime!");

  RtcDateTime now = Rtc.GetDateTime();
  printDateTime(now);
  Serial.println();

  for (int i = 0; i < numberOfFeedTimes * 3; i = i + 3) {
    int thu = 0, gio = 0, phut = 0, chedo = 0, cong = 0, onoff = 0;
    readThuGioPhut(thu, gio, phut, feedTime[i + 1]);
    readCheDoCongRaOnOff(chedo, cong, onoff, feedTime[i + 2]);

    if(now.Hour() != gio || now.Minute() != phut || onoff != 1 || now.DayOfWeek() != thu)
      continue;
    else 
    {
      if(chedo == 1 && now.Second() < 5) {
          digitalWrite(cong, HIGH);
          delay(5000);
          digitalWrite(cong, LOW); 
      }
      
      if(chedo == 2 && now.Second() < 10) {
          digitalWrite(cong, HIGH);
          delay(1000);
          digitalWrite(cong, LOW); 
          delay(1000);          
      }
    }
    
  }
    
  delay(1000);
}
