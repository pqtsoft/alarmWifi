import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { NavigationActions, StackNavigator } from 'react-navigation';
import Home from './App/Container/Home';
import Setting from './App/Container/Setting';

export default StackNavigator({
  home: { screen: Home },
  setting: { screen: Setting }
}, {
  headerMode: 'none'
});