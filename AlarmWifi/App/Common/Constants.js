import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');

export default {
  baseURL: 'http://192.168.4.22/',
  width,
  height,
  shadow: {
    backgroundColor: '#FFFFFF',
    ...Platform.select({
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowRadius: 2,
        shadowOpacity: 1,
      },
      android: {
        elevation: 2,
      },
    }),
  },
}