/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App/Container/Home';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
