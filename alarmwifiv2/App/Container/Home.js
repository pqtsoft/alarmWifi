import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  TimePickerAndroid,
  Platform,
  ToastAndroid,
  Switch,
  ActivityIndicator,
  TextInput,
  Picker,
} from 'react-native';
import axios from 'axios';
import Constants from '../Common/Constants';
import ScrollableTabView, {
  ScrollableTabBar,
} from 'react-native-scrollable-tab-view';

const month = [
  'THÁNG 1',
  'THÁNG 2',
  'THÁNG 3',
  'THÁNG 4',
  'THÁNG 5',
  'THÁNG 6',
  'THÁNG 7',
  'THÁNG 8',
  'THÁNG 9',
  'THÁNG 10',
  'THÁNG 11',
  'THÁNG 12',
];
const strPort = 'Cổng ';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: JSON.parse(
        '[{"id":1,"dayOfWeek":1,"hour":1,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":2,"dayOfWeek":1,"hour":4,"minute":0,"chedo":1,"cong":1,"onoff":0},{"id":3,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":4,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":5,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":6,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":7,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":8,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":9,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":10,"dayOfWeek":1,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":11,"dayOfWeek":2,"hour":23,"minute":45,"chedo":1,"cong":16,"onoff":1},{"id":12,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":13,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":14,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":15,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":16,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":17,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":18,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":19,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":20,"dayOfWeek":2,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":21,"dayOfWeek":3,"hour":12,"minute":0,"chedo":1,"cong":16,"onoff":0},{"id":22,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":23,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":24,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":25,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":26,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":27,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":28,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":29,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":30,"dayOfWeek":3,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":31,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":32,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":33,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":34,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":35,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":36,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":37,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":38,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":39,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":40,"dayOfWeek":4,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":41,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":42,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":43,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":44,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":45,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":46,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":47,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":48,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":49,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":50,"dayOfWeek":5,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":51,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":52,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":53,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":54,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":55,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":56,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":57,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":58,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":59,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":60,"dayOfWeek":6,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":61,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":62,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":63,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":64,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":65,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":66,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":67,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":68,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":69,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1},{"id":70,"dayOfWeek":7,"hour":0,"minute":0,"chedo":1,"cong":16,"onoff":1}]',
      ),
      isConnected: false,
      currentTime: '--:--:--',
      day: new Date().getDate(),
      month: new Date().getMonth(),
      refreshing: false,
      isLoading: false,
      isLongPressOption: false,
      currentLongPressItem: null,
      currentCong: 0,
      currentCheDo: 1,
    };
    this.intervalRemain = setInterval(() => {
      this.checkConnect();
    }, 10000);

    this.stringDayOfWeek = [
      {name: 'Thứ hai', id: 1},
      {name: 'Thứ ba', id: 2},
      {name: 'Thứ tư', id: 3},
      {name: 'Thứ năm', id: 4},
      {name: 'Thứ sáu', id: 5},
      {name: 'Thứ bảy', id: 6},
      {name: 'Chủ nhật', id: 0},
    ];
    this.mode = ['', 'Reo 1 lần', 'Lặp lại ngắt quãng'];

    this.renderItem = this.renderItem.bind(this);
    this.onPickTime = this.onPickTime.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onSave = this.onSave.bind(this);
    this.checkConnect = this.checkConnect.bind(this);
    this.onChangeSwitch = this.onChangeSwitch.bind(this);
    this.onLongPress = this.onLongPress.bind(this);
    this.renderLongPressBox = this.renderLongPressBox.bind(this);
    this.changeItem = this.changeItem.bind(this);
  }

  refresh() {
    this.setState({
      refreshing: true,
      isLoading: true,
    });
    axios
      .get(Constants.baseURL + 'all', {
        timeout: 10000,
      })
      .then((response) => {

      this.setState({
          data: response.data,
          refreshing: false,
          isLoading: false,
          isSetting: false,
        });
      })
      .catch((err) => {
        console.log(err);
        ToastAndroid.show(
          'Có lỗi xảy ra, không thể cập nhật!',
          ToastAndroid.SHORT,
        );
        this.setState({
          refreshing: false,
          isLoading: false,
          isSetting: false,
        });
      });
  }

  componentDidMount() {
    this.checkConnect();
    this.refresh();
  }

  checkConnect() {
    let isConnected = false;
    let currentTime = '--:--';
    axios
      .get(Constants.baseURL + 'check', {
        timeout: 10000,
      })
      .then((response) => {
        const data = response.data;
        if (data.device == 'IOTALARM') {
          currentTime = data.time;
          isConnected = true;
        }


        if( this.state.isConnected !== isConnected ||
          this.state.currentTime != data.time
        ) {
          if (isConnected === true) {
            this.refresh();
          }
          this.setState({
            isConnected,
            currentTime,
          });
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          isConnected: false,
          currentTime,
        });
      });
  }

  _keyExtractor = (item, index) => item.id;

  requestSet({id, hour, minute, dayOfWeek, boolOnOff, chedo, cong}) {
    this.setState({
      isSetting: true,
    });
    hour = hour < 10 ? '0' + hour : hour;
    minute = minute < 10 ? '0' + minute : minute;
    hourminutedayOfWeek = hour + '' + minute + '' + dayOfWeek;

    chedocongonoff =
      chedo +
      '' +
      (cong < 10 ? '0' + cong : cong) +
      '' +
      (boolOnOff == true ? '1' : '0');
    console.log(
      Constants.baseURL +
        'set' +
        '?id=' +
        id +
        '&thugiophut=' +
        hourminutedayOfWeek +
        '&chedocongonoff=' +
        chedocongonoff,
    );
    axios
      .get(
        Constants.baseURL +
          'set' +
          '?id=' +
          id +
          '&thugiophut=' +
          hourminutedayOfWeek +
          '&chedocongonoff=' +
          chedocongonoff,
        {
          timeout: 4000,
        },
      )
      .then((response) => {
        this.refresh();
        ToastAndroid.show('Cập nhật thành công!', ToastAndroid.SHORT);
      })
      .catch((err) =>
        ToastAndroid.show(
          'Có lỗi xảy ra, không thể cập nhật!',
          ToastAndroid.SHORT,
        ),
      );
  }

  onDelete(id) {
    this.requestSet({id, hour: 255, minute: 255, dayOfWeek: 9});
  }

  onSave() {}

  changeItem(id, value) {
    const item = this.state.data.find((x) => x.id === id);
    if  (item !== undefined) {
      var stateCopy = Object.assign({}, this.state);
      stateCopy.data = stateCopy.data.slice();
      stateCopy.data[this.state.data.indexOf(item)] = Object.assign(
        {},
        stateCopy.data[this.state.data.indexOf(item)],
      );
      stateCopy.data[this.state.data.indexOf(item)] = value;

      this.setState(stateCopy);
    }
  }

  async onPickTime(
    id,
    inputhour,
    inputminute,
    dayOfWeek,
    boolOnOff,
    chedo,
    cong,
  ) {
    try {
      const {action, hour, minute} = await TimePickerAndroid.open({
        hour: inputhour !== 255 ? inputhour : null,
        minute: inputminute !== 255 ? inputminute : null,
        is24hour: false,
      });

      if (action !== TimePickerAndroid.dismissedAction) {
        this.changeItem(id, {
          id,
          hour,
          minute,
          dayOfWeek,
          onoff: boolOnOff,
          chedo,
          cong
        });

        this.requestSet({
          id,
          hour,
          minute,
          dayOfWeek,
          boolOnOff,
          chedo,
          cong
        });
      }
    } catch ({code, message}) {
      console.warn('Cannot open time picker', message);
    }
  }

  renderTime(hour, minute) {
    hour = hour < 10 ? '0' + hour : hour;
    minute = minute < 10 ? '0' + minute : minute;

    if  (hour === 255 || minute === 255) return null;
    return (
      <Text style={{fontSize: 35, color: '#14828f'}}>
        {hour} : {minute}
      </Text>
    );
  }

  onChangeSwitch(item) {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.data = stateCopy.data.slice();
    stateCopy.data[this.state.data.indexOf(item)] = Object.assign(
      {},
      stateCopy.data[this.state.data.indexOf(item)],
    );
    stateCopy.data[this.state.data.indexOf(item)] = {
      ...item,
      onoff: !item.onoff,
    };
    this.requestSet({
      id: item.id,
      hour: item.hour,
      minute: item.minute,
      dayOfWeek: item.dayOfWeek,
      boolOnOff: item.onoff == 1 ? 0 : 1,
      chedo: item.chedo,
      cong: item.cong
    });
  }

  onLongPress(e, item) {
    this.setState({
      isLongPressOption: true,
      currentLongPressItem: item,
      currentCong: item.cong,
      currentCheDo: item.chedo
    });
  }

  renderItem({key, item}) {
    const mode = this.mode[item.chedo];;
    return (
      <View style={{flex: 1, flexDirection: 'column', padding: 20}}>
        <Text style={{color: '#14828f'}}>{mode}</Text>
        <View style={{flex: 0.8, flexDirection: 'row'}}>
          <TouchableOpacity
            delayLongPress={300}
            onLongPress={(e) => this.onLongPress(e, item)}
            style={{flex: 8}}
            onPress={() =>
              Platform.OS === 'android'
                ? this.onPickTime(
                    item.id,
                    item.hour,
                    item.minute,
                    item.dayOfWeek,
                    item.onoff,
                  )
                : {}
            }>
            {this.renderTime(item.hour, item.minute)}
          </TouchableOpacity>
          {/* <TouchableOpacity onPress={() => this.onDelete(item.id)}>
        <MaterialCommunityIcons name="delete" color="#777777" size={30} />
      </TouchableOpacity> */}
          <Switch
            style={{transform: [{scaleX: 1.5}, {scaleY: 1.5}]}}
            trackColor="#00a87f"
            dayOfWeekmbTintColor="#FFF"
            value={item.onoff == 1}
            onValueChange={() => this.onChangeSwitch(item)}
          />
        </View>
      </View>
);
  }

  renderLongPressBox() {
    if  (this.state.currentLongPressItem === null) {
      return null;
    }
    const item = this.state.currentLongPressItem;
    return (
      <View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          zIndex: 100,
          padding: 20,
          backgroundColor: '#34343477',
          justifyContent: 'center',
          alignItems: 'center',
          width: Constants.width,
          height: Constants.height,
        }}>
        <View
          style={[
            Constants.shadow,
            {
              backgroundColor: '#fff',
              paddingHorizontal: 64,
              paddingVertical: 20,
              height: 220,
              width: 300,
            },
          ]}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Text style={{color: '#14828f'}}>Chọn cổng ra</Text>
            <Picker
              style={{width: 200}}
              selectedValue={this.state.currentCong}
              onValueChange={(value) => this.setState({currentCong: value})}>
              <Picker.Item label={strPort + '1'} value={1} />
              <Picker.Item label={strPort + '3'} value={3} />
              <Picker.Item label={strPort + '4'} value={4} />
              <Picker.Item label={strPort + '5'} value={5} />
              <Picker.Item label={strPort + '6'} value={6} />
              <Picker.Item label={strPort + '7'} value={7} />
              <Picker.Item label={strPort + '8'} value={8} />
              <Picker.Item label={strPort + '9'} value={9} />
              <Picker.Item label={strPort + '10'} value={10} />
              <Picker.Item label={strPort + '11'} value={11} />
              <Picker.Item label={strPort + '12'} value={12} />
              <Picker.Item label={strPort + '13'} value={13} />
              <Picker.Item label={strPort + '14'} value={14} />
              <Picker.Item label={strPort + '15'} value={15} />
              <Picker.Item label={strPort + '16'} value={16} />
            </Picker>
            <Text style={{color: '#14828f'}}>Chọn chế độ</Text>
            <Picker
              style={{width: 200}}
              selectedValue={this.state.currentCheDo}
              onValueChange={(value) => this.setState({currentCheDo: value})}>
              <Picker.Item label={'Reo 1 lần'} value={1} />
              <Picker.Item label={'Lặp lại ngắt quãng'} value={2} />
            </Picker>
          </View>
          <View
            style={{
              flexDirection: 'row',
              margin: 20,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <TouchableOpacity
              onPress={() => this.setState({isLongPressOption: false})}>
              <Text style={{marginRight: 100, fontSize: 17}}>Hủy</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.requestSet({
                  ...item,
                  cong: this.state.currentCong,
                  chedo: this.state.currentCheDo,
                });
              }}>
              <Text style={{color: '#14828f', fontSize: 17}}>Lưu</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    console.log(this.stringDayOfWeek);;
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: '#FFF'}}>
          <View
            style={{
              backgroundColor: '#14828f',
              justifyContent: 'center',
              flex: 0.25,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <Text style={{color: '#FFF', fontSize: 15}}>
                {'NGÀY ' + this.state.day + ' - ' + month[this.state.month]}
              </Text>
              <Text style={{color: '#FFF', fontSize: 45}}>
                {this.state.currentTime}
              </Text>
            </View>
          </View>
          {!this.state.isConnected && (
            <View
              style={{
                justifyContent: 'center',
                alignContent: 'center',
                flex: 1,
                flexDirection: 'row',
                backgroundColor: '#ff0000',
                maxHeight: 20,
              }}>
              <Text style={{color: '#fff', fontSize: 14}}>Not Connected</Text>
            </View>
          )}
          <ScrollableTabView
            renderTabBar={() => (
              <ScrollableTabBar
                activeTextColor="#14828f"
                underlineStyle={{backgroundColor: '#14828f'}}
              />
            )}>
            {this.stringDayOfWeek.map((item, index) =>(
              <FlatList
                key={index}
                tabLabel={item.name}
                refreshing={this.state.refreshing}
                onRefresh={() => {
                  this.refresh();
                }}
                data={
                  this.state.data !== null
                    ? this.state.data.filter((x) => x.dayOfWeek === item.id)
                    : []
                }
                renderItem={this.renderItem}
                keyExtractor={this._keyExtractor}
              />
            ))}

            </ScrollableTabView>

        </View>
        {this.state.isLongPressOption && this.renderLongPressBox()}
        {this.state.isSetting && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              zIndex: 100,
              padding: 20,
              backgroundColor: '#34343477',
              justifyContent: 'center',
              alignItems: 'center',
              width: Constants.width,
              height: Constants.height,
            }}>
            <View
              style={[
                Constants.shadow,
                {
                  backgroundColor: '#fff',
                  paddingHorizontal: 64,
                  paddingVertical: 20,
                  borderRadius: 10,
                  flexDirection: 'column',
                  alignItems: 'center',
                },
              ]}>
              <ActivityIndicator size="large" color="#34343477" />
              <Text
                style={{
                  color: '#34343477',
                  fontWeight: 'bold',
                  fontSize: 16,
                  marginTop: 10,
                  fontFamily:
                    Platform.OS == 'android' ? 'Open Sans' : undefined,
                }}>
                Đang cập nhật
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}
